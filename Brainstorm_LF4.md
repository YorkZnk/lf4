# Brainstorm LF4
## Eventuelle Schwachstellen im Netzwerkplan
- Nur eine Firewall für zwei Standorte 
- Nur einen Switch pro Standort (Jeder hat Zugriff auf das ganze Netzwerk inklusive der Server)
- Standleitung zwischen den Standorten (evtl Angriffspunkt)
- Für den Betrieb der IT an allen Standorten ist der HH Ohlsdorf Standort zuständig (keine Redundanz)
- HH Schnelsen hat keine Firewall

## Hypothesen wie es zum Ausfall kommen konnte
- Ein Mitarbeiter hat versehentlich kritische Infrastruktur beeinflusst
- Jemand hat sich über die Standleitung oder einen Switch Zugang verschafft
- Ein Mitarbeiter hat evtl eine Angreifer-Email oder Programm geöffnet und somit den Zugang zum Netzwerk ermöglicht (Virus)
- Überlastung
- Firewall Konfigurierung
- Hardwareausfall / DDoS
- ISP verschuldetes Problem

## Spekulationen zum Angreifer
- Evtl haben Konkurrenten den Angriff ausgeführt um MooveTeq GmhB zu schwächen und selbst Marktanteile zu gewinnen
- Erpresser
- Versehen / Unwissenheit
- Angreifer wollen an die Kundendaten (evtl Bankdaten)